const
	express = require('express'),
	bodyParser = require('body-parser'),
    Analytics = require('anonymous-analytics'),
	fs = require('fs'),
	email = require('./email'),
	app = express(),
    portfolioAnalytics = new Analytics('portfolio');

let config = require('./config');

app.set('view engine', 'ejs');

app.use(bodyParser.json());

app.use('/assets', express.static('assets'));

app.get('*', req => {
	config = JSON.parse(fs.readFileSync('./config.json', 'utf8'));
	req.next();
});

app.post('/:locale?/mail', (request, response) => {
	email(`${request.body.lastName} ${request.body.firstName}`, request.body.mail, request.params.locale || config.author.defaultLocale, request.body.message)
		.then(() => response.json({ success: true }))
		.catch(error => {
			console.error(error);
			response.json({ success: false });
		});
});

app.get('/:locale?/cv', (req, res) => {
	res.setHeader('content-type', 'application/pdf');
	const
		currentLocale = req.params.locale || config.author.defaultLocale,
		file = `./cv/${currentLocale}.pdf`;
	res.setHeader('content-disposition', `inline; filename="tiana_lemesle_cv_${currentLocale}.pdf`);
	res.setHeader('content-length', fs.statSync(file).size);
	fs.createReadStream(file).pipe(res);
});

app.get('/stats', async (request, response) => await response.json(await portfolioAnalytics.getStats(undefined, undefined, true)));

app.get('/:locale?', (req, res) => {
    portfolioAnalytics.addFromExpress(req);
	const
		y1 = config.author.copyright,
		y2 = new Date().getFullYear(),
        requestedLocale = req.params.locale,
        { defaultLocale } = config.author,
        browserLocale = req.acceptsLanguages()[0].split('-')[0];
    let currentLocale;
    if(requestedLocale){
        if(fs.existsSync(`./locales/${requestedLocale}.json`))
            currentLocale = requestedLocale;
        else {
            currentLocale = defaultLocale;
            res.status(404);
        }
    }
    else {
        if(fs.existsSync(`./locales/${browserLocale}.json`))
            currentLocale = browserLocale;
        else
            currentLocale = defaultLocale;
    }
    const locale = JSON.parse(fs.readFileSync(`./locales/${currentLocale}.json`, 'utf8')
        .replace(/%age%/g, (new Date(new Date() - new Date(config.author.birthdate)).getUTCFullYear() - 1970).toString())
        .replace(/%phone%/g, config.author.phone)
        .replace(/%mail%/g, config.email.recipient)
        .replace(/\[(.+?)]\((.+?)\)/g, '<a class=\\"link\\" href=\\"$2\\" target=\\"_blank\\">$1</a>')
    );
	res.render('index', {
		favicon: config.author.favicon,
		name: config.author.name,
		age: new Date(Date.now() - new Date(config.author.birthdate).getTime()).getUTCFullYear() - 1970,
		mail: config.email.recipient,
		currentLocale,
		locale,
		locales: fs.readdirSync('./locales')
			.map(file => file.split('.')[0])
			.filter(file => file !== 'example'),
		copyYear: y1 === y2 ? y2 : `${y1} — ${y2}`
	});
});

app.get(
	'/:locale/:_(skill|org|education)/:id',
	(req, res) => {
		res.render(
			'under_construction',
			{
				favicon: config.author.favicon,
				name: config.author.name,
                homepageUrl: `/${req.params.locale}`,
				...{
					'en': {
						heading: `Portfolio under redesign`,
						message1: `Thank you for clicking a hyperlink from my redesigned resume.`,
						message2: `Unfortunately, the redesigned portfolio that comes with it isn't ready yet.`,
						message3: `Please revisit in a few weeks.`,
						homepage: `Old portfolio homepage`
					},
					'fr': {
						heading: `Portfolio en cours de refonte`,
						message1: `Merci d'avoir cliqué un lien sur mon nouveau CV.`,
						message2: `Malheureusement, le nouveau portfolio allant avec n'est pas encore prêt.`,
						message3: `Revenez s'il vous plaît dans quelques semaines.`,
						homepage: `Accueil de l'ancien portfolio`
					}
				}[req.params.locale]
			}
		);
	}
);

app.listen(config.httpPort);
