const
	config = require('./config'),
	nodemailer = require('nodemailer'),
	transporter = nodemailer.createTransport({
		host: config.email.sender.host,
		port: config.email.sender.port,
		secure: true,
		auth: {
			user: config.email.sender.address,
			pass: config.email.sender.password
		}
});

module.exports = (name, mail, locale, message, recipient) => new Promise((resolve, reject) => transporter.sendMail({
	from: config.email.sender.address,
	to: recipient || config.email.recipient,
	replyTo: mail,
	subject: `[Portfolio Contact Form] [${locale}] ${name}`,
	text: message
}, (error, info) => {
	if(error)
		reject(error);
	else
		resolve(info);
}));
