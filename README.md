# tianalemesle.fr

[![](https://shields.kaki87.net/endpoint?url=https%3A%2F%2Fraw.githubusercontent.com%2Fserver-KaTys%2Fstatus%2Fmaster%2Fapi%2Ftiana-lemesle-portfolio%2Fuptime.json)](https://status.katys.eu.org/history/tiana-lemesle-portfolio)
[![](https://shields.kaki87.net/discord/739600823415472128)](https://discord.gg/qexFe6b)

[![CC-BY-SA-4.0](https://tianalemesle.fr/assets/img/CC-BY-SA-4.0.png)](https://creativecommons.org/licenses/by-sa/4.0/)

My portfolio

## Getting started

### Prerequisites

- NodeJS
- NPM
- Yarn

### Install

```
git clone https://git.kaki87.net/KaKi87/tianalemesle.fr.git <example.com>
cd <example.com>
yarn install
```

### Use

- Copy `config.example.json` as `config.json` and fill it
- Start the server `node app.js`
- Fill your website with your own content in `/locales` and `/cv`

### Credits

- [Pierre Grangereau](https://pierre.grangereau.fr/) : web design

### Copyright

The code is released under Creative Commons BY-SA 4.0 license.

The web designer's copyright must stay visible on the website's homepage.