document.querySelector('.header__scroll').onclick = () => window.scroll({
	behavior: 'smooth',
	left: 0,
	top: document.querySelector('.main').offsetTop
});

document.querySelectorAll('.skill__card').forEach(e => e.onclick = e => {
	document.querySelector('.skill--selected').classList.remove('skill--selected');
	const skill = e.target.parentElement;
	skill.classList.add('skill--selected');
	document.querySelector('.skills__current-description').innerHTML = skill.querySelector('.skill__description').innerHTML;
});

document.querySelector('.contact__form').onsubmit = e => {
	e.preventDefault();
	const
		req = new XMLHttpRequest(),
		lastName = document.querySelector('.contact__lastName').value,
		firstName = document.querySelector('.contact__firstName').value,
		mail = document.querySelector('.contact__mail').value,
		message = document.querySelector('.contact__message').value,
        submit = document.querySelector('.contact__send');
	if(!lastName || !firstName || !mail || !message) return;
	req.open('POST', 'mail', true);
	req.setRequestHeader('content-type', 'application/json');
	req.onreadystatechange = function (){
		if(this.readyState === XMLHttpRequest.DONE && this.status === 200){
			const
                res = JSON.parse(this.response);
			if(res.success){
                submit.value = submit.getAttribute('data-sent');
                [...document.querySelectorAll('.contact__form *:not([type=submit])')].filter(el => !!el.value).forEach(el => el.value = '');
                setTimeout(() => submit.value = submit.getAttribute('data-send'), 3000);
            }
			else
			    submit.value = submit.getAttribute('data-failed');
		}
	};
	req.send(JSON.stringify({ lastName, firstName, mail, message }));
	submit.value = submit.getAttribute('data-sending');
};