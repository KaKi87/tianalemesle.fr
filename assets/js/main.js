document.querySelector('.menu__open').onclick = () => document.querySelector('.menu').classList.toggle('menu--active');

document.querySelector('.menu__list').addEventListener('click', () => document.querySelector('.menu').classList.toggle('menu--active'));

window.onscroll = () => {
	const
		scrollTop = document.querySelector('.scroll-top'),
		currentScrollSection = [...document.querySelectorAll('body > .white-on-blue, body > .blue-on-white')]
			.reverse()
			.filter(el => window.innerHeight - (el.offsetTop - window.pageYOffset) > 0)[0];
	if(window.scrollY > 20)
		scrollTop.classList.add('scroll-top--active');
	else
		scrollTop.classList.remove('scroll-top--active');
	scrollTop.classList.remove('white-on-blue', 'blue-on-white');
	if(currentScrollSection.classList.contains('white-on-blue')){
		scrollTop.classList.add('blue-on-white');
	}
	if(currentScrollSection.classList.contains('blue-on-white')){
		scrollTop.classList.add('white-on-blue');
	}
};

document.querySelector('.scroll-top').onclick = () => window.scroll({
	behavior: 'smooth',
	left: 0,
	top: 0
});